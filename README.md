# GreaseMonkey - for Ridibooks
## Ridibooks display additional information
리디북스 구매한 책 마킹 및 할인날짜 표시

[Source](https://bitbucket.org/hwiorn/gm-ridibooks/src/master/GreaseMonkey_ridibooks_display_additional_information.user.js?fileviewer=file-view-default)

[Install](https://bitbucket.org/hwiorn/gm-ridibooks/raw/master/GreaseMonkey_ridibooks_display_additional_information.user.js)

## Ridibooks add additional buttons
리디북스 카트에 '선택', '정렬' 버튼 추가

[Source](https://bitbucket.org/hwiorn/gm-ridibooks/src/master/GreaseMonkey_ridibooks_additional_buttons.user.js?fileviewer=file-view-default)

[Install](https://bitbucket.org/hwiorn/gm-ridibooks/raw/master/GreaseMonkey_ridibooks_additional_buttons.user.js)
