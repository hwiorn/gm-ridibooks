// ==UserScript==
// @name        Ridibooks display additional information
// @description 리디북스에서 구매한 책 표시 및 할인날짜 표시.
// @namespace   https://bitbucket.org/hwiorn/gm-ridibooks
// @include     *ridibooks.com*
// @grant       GM_getValue
// @grant       GM_setValue
// @icon        http://static.ridibooks.com/books/images/favicon/ridibooks.ico
// @version     1.5
// @updateURL   https://bitbucket.org/hwiorn/gm-ridibooks/raw/master/GreaseMonkey_ridibooks_display_additional_information.user.js
// @downloadURL https://bitbucket.org/hwiorn/gm-ridibooks/raw/master/GreaseMonkey_ridibooks_display_additional_information.user.js
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// ---------------------------
// ChangeLog
// v1.4
// - Menu link 동작 안되는 것 제거
// - [구매완료] 표시 안되는 문제 수정
// - 할인날짜 표시 안되는 문제 수정
// v1.2
// - 체험판/무료 책에 구매완료 표시되는 문제 수정.
// v1.1
// - 구매한 책 및 할인날짜 표시.
// ==/UserScript==

/* Check the books which are bought */
function _mark_b(thumb, text) {
      $(text).prepend('[구매완료]');
      $(text).css({
        'text-decoration': 'line-through',
        'color': '#999',
      });
     $(thumb).css({
        'filter': 'alpha(opacity=40)',
        'opacity': '0.4'
      });
}

// 책 소개 페이지에서 구매 완료 출력
$(".detail_header").each(function() {
    var id = $(this).find('div.book_thumbnail_wrapper').attr('data-book_id_for_tracking');
    var text = $(this) .find('.info_title_wrap');
    var thumb = $(this) .find('div.thumbnail_image');
    var btn = $(this).find('.js_download_book');
    if (btn.length > 0 && btn.attr('data-type') !== 'free') {
        _mark_b(thumb, text);
	      GM_setValue(id, 'bought');
	  }
});

// 책 목록에서 구매완료 출력
$('.book_macro_metadata_portrait, .book_macro_portrait, .bookmacro_wrapper').each(function () {
    var id = $(this).find('div.book_thumbnail_wrapper').attr('data-book_id_for_tracking');
    var text = $(this) .find('span.title_text');
    var thumb = $(this) .find('div.thumbnail_image');
    var book = GM_getValue(id);

  if(book === 'bought') {
    _mark_b($(thumb), $(text));
  } else {
      if(id !== undefined) {
          $.ajax({
              url: '/v2/Detail',
              data: {'id': id},}).done(function (d) {
                  var x = /<button.+class=(['"].*btn_all_buy.*['"]\s+.*)>/.exec(d);
                  if (x && x[0].search('js_download_book') >= 0 && x[0].search('free') < 0) {
                      _mark_b(thumb, text);
	                    GM_setValue(id, 'bought');
	                }
	            });
      }
  }
});


/* Cart, Wishlist(상세보기) 할인 날짜 표시 */
$('.book_macro_metadata_portrait') .each(function () {
    var pwrap = $(this) .find('.book_price_wrapper');
    var id = $(pwrap).attr('data-book-id');
    var lp = $(pwrap).find('.ebook_price');

    if(lp.length > 0) {
        if(id !== undefined) {
            $.ajax({
                url: '/v2/Detail',
                data: {'id': id},
            }) .done(function (d) {
                var x = /<p class=['"]info_discount_period_wrap['"]>\n.+\n\s*<\/p>/.exec(d);
                if (x)
                    pwrap.append(x[0]);
            });
        }
    }
});

