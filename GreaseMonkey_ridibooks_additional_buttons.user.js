// ==UserScript==
// @name         Ridibooks add additional buttons
// @namespace    https://bitbucket.org/hwiorn/gm-ridibooks
// @description 리디북스 카트에 '선택', '정렬' 버튼 추가
// @include     *ridibooks.com/v2/Cart*
// @icon http://static.ridibooks.com/books/images/favicon/ridibooks.ico
// @version     1.3
// @updateURL https://bitbucket.org/hwiorn/gm-ridibooks/raw/master/GreaseMonkey_ridibooks_additional_buttons.user.js
// @downloadURL https://bitbucket.org/hwiorn/gm-ridibooks/raw/master/GreaseMonkey_ridibooks_additional_buttons.user.js
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// ==/UserScript==

//ChangeLog
// 1.3
// - 정렬, 선택 수정

//Cart select button
[$("<button type='button' id='select_all_discount' class='default_btn btn_white'>할인선택</button>"),
 $("<button type='button' id='select_all_bookset' class='default_btn btn_white'>세트선택</button>"),
 $("<button type='button' id='select_all_under_1price' class='default_btn btn_white'>1만원이하</button>"),
 $("<button type='button' id='select_all_under_2price' class='default_btn btn_white'>2만원이하</button>"),
 $("<button type='button' id='select_all_under_3price' class='default_btn btn_white'>3만원이하</button>"),
 $("<button type='button' id='select_all_under_5price' class='default_btn btn_white'>5만원이하</button>"),
].forEach(function(e,a,i) {
    e.click(function(e) {selectAllBooksInCart($(this));});
    //FIXME: 개행 필요
    if($(".buttons_wrapper")) {
        $(".buttons_wrapper").append(e);
    }
});

//Cart sort button
[$("<button type='button' id='sort_price' class='default_btn btn_blue' text='가격'></button>"),
 //FIXME: 할인율은 이제 필요없지 않나?
 // $("<button type='button' id='sort_discount' class='default_btn btn_blue' text='할인율'></button>"),
 $("<button type='button' id='sort_title' class='default_btn btn_blue' text='제목'></button>"),
 $("<button type='button' id='sort_author' class='default_btn btn_blue' text='저자'></button>"),
 $("<button type='button' id='sort_set' class='default_btn btn_blue' text='세트'></button>"),
].forEach(function(e) {
    if($(".buttons_wrapper")) {
        e.click(function(e) {sortBooksInCart($(this));});
        $(".buttons_wrapper").append(e);
    }
});

function cleanSortButtons() {
    [ $("#sort_price"),
      $("#sort_discount"),
      $("#sort_title"),
      $("#sort_author"),
      $("#sort_set"),
    ].forEach(function(e) {
        $(e).attr('ascending', false);
        $(e).text($(e).attr('text'));
    });
}

function sortBooksInCart(btn) {
    $(btn).attr("disabled",true);
    var asc = ($(btn).attr("ascending") === "true");
    //var p = $(".bookmacro_wrapper.book_list_110");
    var p = $(".bookmacro_wrapper");
    var f = null;
    switch(btn.attr("id")) {
    case 'sort_author':
        f = p.find('div.book_macro_metadata_portrait').sort(function(a,b) {
            var titleA = $(a).find('.author').text().replace(/\s|\n|<[^>]+>/g,"").toUpperCase();
            var titleB = $(b).find('.author').text().replace(/\s|\n|<[^>]+>/g,"").toUpperCase();
            return asc ? titleA.localeCompare(titleB) : titleB.localeCompare(titleA);
        });
        break;
    case 'sort_title':
        f = p.find('div.book_macro_metadata_portrait').sort(function(a,b) {
            var titleA = $(a).find('.title_text').text().toUpperCase();
            var titleB = $(b).find('.title_text').text().toUpperCase();
            return asc ? titleA.localeCompare(titleB) : titleB.localeCompare(titleA);
        });
        break;
    case 'sort_price':
        f = p.find('div.book_macro_metadata_portrait').sort(function(a,b) {
            var r = /^[0-9,]+/;
            var _a = $(a).find('.ebook_price');
            if(_a.length == 0)
                _a = $(a).find('.coupon_price');

            var _b = $(b).find('.ebook_price');
            if(_b.length == 0)
                _b = $(b).find('.coupon_price');
            var priceA = r.exec($(_a).find('.museo_sans')[0].innerHTML)[0].replace(/,/g,"");
            var priceB = r.exec($(_b).find('.museo_sans')[0].innerHTML)[0].replace(/,/g,"");
            if(asc)
                return (priceA < priceB) ? -1 : (priceA > priceB) ? 1 : 0;
            else
                return (priceA < priceB) ? 1 : (priceA > priceB) ? -1 : 0;
        });
        break;
    case 'sort_discount':
        f = p.find('div.book_macro_metadata_portrait').sort(function(a,b) {
            var r = /\((\d+)%\)/;
            var NonDiscountA = $(a).find('.discount_percent').find('.discount_none').length > 0;
            var NonDiscountB = $(b).find('.discount_percent').find('.discount_none').length > 0;
            if(NonDiscountA && NonDiscountB) {
                return 0;
            }

            var priceA = NonDiscountA ? 99999999999 : $(a).find('.coupon_price').find('strong').find('.museo_sans')[0].innerHTML.replace(/,/g,"");
            var priceB = NonDiscountB ? 99999999999 : $(b).find('.coupon_price').find('strong').find('.museo_sans')[0].innerHTML.replace(/,/g,"");
            priceA = priceA ? parseInt(priceA[1]) : 0;
            priceB = priceB ? parseInt(priceB[1]) : 0;
            if(asc)
                return (priceA < priceB) ? -1 : (priceA > priceB) ? 1 : 0;
            else
                return (priceA < priceB) ? 1 : (priceA > priceB) ? -1 : 0;
        });
        break;
    case 'sort_set':
        f = p.find('div.book_macro_metadata_portrait').sort(function(a,b) {
            var setA = $(a).find('.set_book').length > 0;
            var setB = $(b).find('.set_book').length > 0;

            if(setA && setB) {
                var titleA = $(a).find('.title_text')[0].innerHTML.toUpperCase();
                var titleB = $(b).find('.title_text')[0].innerHTML.toUpperCase();
                return asc ? titleA.localeCompare(titleB) : titleB.localeCompare(titleA);
            }
            else {
                return setA ? (asc ? -1 : 1) : (asc ? 1 : -1);
            }
        });
        break;
    default:
        return;
    }
    if(f)
        p.append(f);
    cleanSortButtons();
    $(btn).text($(btn).attr("text") + (asc ? "▲" : "▼"));
    $(btn).attr("ascending", !asc);
    $(btn).attr("disabled",false);
}

function selectAllBooksInCart(btn) {
    $(btn).attr("disabled",true);
    if($("input.js_checkbox_all")[0].checked)
        $("input.js_checkbox_all")[0].click();
    switch(btn.attr("id")) {
    case 'select_all_discount':
        $('div.book_macro_metadata_portrait').each(function(i,e) {
            var c = $(e).find('.thumbnail_checkbox > input')[0];
            if($(e).find('.discount_percent').length > 0 && !c.checked)
                c.click();
        });
        break;
    case 'select_all_bookset':
        $('div.book_macro_metadata_portrait').each(function(i,e) {
            var c = $(e).find('.thumbnail_checkbox > input')[0];
            if($(e).find('.set_book').length > 0 && !c.checked)
                c.click();
        });
        break;
    case 'select_all_under_1price':
        $('div.book_macro_metadata_portrait').each(function(i,e) {
            var r = /^[0-9,]+/;
            var c = $(e).find('.thumbnail_checkbox > input')[0];
            var d = parseInt(r.exec($(e).find('.coupon_price').find('.museo_sans')[0].innerHTML)[0].replace(/,/g,""));
            if(d <= 10000 && !c.checked)
                c.click();
        });
        break;
    case 'select_all_under_2price':
        $('div.book_macro_metadata_portrait').each(function(i,e) {
            var r = /^[0-9,]+/;
            var c = $(e).find('.thumbnail_checkbox > input')[0];
            var d = parseInt(r.exec($(e).find('.coupon_price').find('.museo_sans')[0].innerHTML)[0].replace(/,/g,""));
            if(d <= 20000 && !c.checked)
                c.click();
        });
        break;
    case 'select_all_under_3price':
        $('div.book_macro_metadata_portrait').each(function(i,e) {
            var r = /^[0-9,]+/;
            var c = $(e).find('.thumbnail_checkbox > input')[0];
            var d = parseInt(r.exec($(e).find('.coupon_price').find('.museo_sans')[0].innerHTML)[0].replace(/,/g,""));
            if(d <= 30000 && !c.checked)
                c.click();
        });
        break;
    case 'select_all_under_5price':
        $('div.book_macro_metadata_portrait').each(function(i,e) {
            var r = /^[0-9,]+/;
            var c = $(e).find('.thumbnail_checkbox > input')[0];
            var d = parseInt(r.exec($(e).find('.coupon_price').find('.museo_sans')[0].innerHTML)[0].replace(/,/g,""));
            if(d <= 50000 && !c.checked)
                c.click();
        });
        break;
    default:
        return;
    }
    $(btn).attr("disabled",false);
}

cleanSortButtons();
